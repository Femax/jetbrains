import com.google.gson.Gson
import com.test.ktor.service.FileServiceImpl
import io.ktor.application.install
import io.ktor.gson.gson
import io.ktor.http.cio.websocket.Frame
import io.ktor.http.cio.websocket.readText
import io.ktor.routing.routing
import io.ktor.server.testing.withTestApplication
import io.ktor.websocket.WebSockets
import io.ktor.websocket.webSocket
import kotlinx.coroutines.launch
import org.junit.Test
import java.nio.file.Paths

class WebSocketTest {

    val gson = Gson();
    @Test
    fun testWatch() {
        withTestApplication {
            application.install(WebSockets)
            val path = Paths.get(System.getProperty("user.home"))
            val fileService = FileServiceImpl();
            application.install(io.ktor.features.ContentNegotiation) {
                gson {}
            }
            application.routing {
                webSocket("/watchDirectory") {
                    for (frame in incoming) {
                        when (frame) {
                            is Frame.Text -> {
                                val path = frame.readText()
                                fileService
                                    .watchDirectory(path)
                                    .map { fileService.getFileList(path) }
                                    .subscribe {
                                        launch {
                                            send(Frame.Text(gson.toJson(it)))
                                        }
                                    }
                                if (path.equals("bye", ignoreCase = true)) {
                                    close()
                                }
                            }
                        }
                    }
                }
            }
            handleWebSocketConversation("/watchDirectory") { incoming, outgoing ->
                val textMessages = listOf(path.toString())
                for (msg in textMessages) {
                    outgoing.send(Frame.Text(msg))
                }
                while (true) {
                    val message = (incoming.receive() as Frame.Text).readText();
                    println(message)
                }
            }
        }
    }

    fun testProfile() {
        withTestApplication {
            application.install(WebSockets)
            val path = Paths.get(System.getProperty("user.home"))
            val fileService = FileServiceImpl();

            application.routing {
                webSocket("/profile") {
                    for (frame in incoming) {
                        when (frame) {
                            is Frame.Text -> {
                                val path = frame.readText()
                                fileService
                                    .profilePath(path)
                                    .map { }
                                if (path.equals("bye", ignoreCase = true)) {
                                    close()
                                }
                            }
                        }
                    }
                }
            }
            handleWebSocketConversation("/profile") { incoming, outgoing ->
                val textMessages = listOf(path.toString())
                for (msg in textMessages) {
                    outgoing.send(Frame.Text(msg))
                    println((incoming.receive() as Frame.Text).toString())
                }
            }
        }
    }
}
