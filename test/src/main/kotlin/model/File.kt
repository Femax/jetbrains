package com.test.ktor.model

import java.util.*

data class FileModel(
    val fileName: String,
    val filePath: String,
    val isDirectory: Boolean,
    val creationTime: Date,
    val size: Long
)

data class FileModelProfile(
    val file: FileModel,
    val profileStatus: FileProfileStatus
)

enum class FileProfileStatus {
    SUCCESS, WARNING, MUST_BE_DELETED
}
