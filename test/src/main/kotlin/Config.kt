package com.test.ktor

import java.nio.file.Paths

object Config {
    val creationTimeConstant = 0.000000000009;
    val fileSizeConstant = 0.00000000009;
    val basePath = Paths.get(System.getProperty("user.home"));
}
