package  com.test.ktor.utils

import java.io.File

object FileUtils {
    fun getFileSizeMegaBytes(file: File): String {
        return (file.length().toDouble() / (1024 * 1024)).toString() + " mb"
    }

    fun getFileSizeKiloBytes(file: File): String {
        return (file.length().toDouble() / 1024).toString() + "  kb"
    }

    fun getFileSizeKiloBytes(file: Long): String {
        return (file.toDouble() / 1024).toString() + "  kb"
    }

    fun getFileSizeBytes(file: File): String {
        return file.length().toString() + " bytes"
    }
}
