package  com.test.ktor.service

import com.test.ktor.model.FileModel
import com.test.ktor.model.FileModelProfile
import io.reactivex.Observable
import java.nio.file.WatchEvent

interface FileService {
    fun getFileList(path: String): List<FileModelProfile>
    fun profilePath(path: String): List<FileModelProfile>
    fun watchDirectory(path: String): Observable<WatchEvent<*>>?
}
