package  com.test.ktor.service

import com.test.ktor.model.FileModel
import com.test.ktor.model.FileModelProfile
import com.test.ktor.model.FileProfileStatus
import de.helmbold.rxfilewatcher.PathObservables
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.WatchEvent
import java.nio.file.attribute.BasicFileAttributes
import java.util.*


class FileServiceImpl : FileService {
    //returns Files with bad
    //return type must say method will be async
    override fun profilePath(path: String): List<FileModelProfile> {
        return (File(path).listFiles()
            ?.toList()
            ?.map {
                val attrs = Files.readAttributes(it.toPath(), BasicFileAttributes::class.java);
                FileModelProfile(
                    FileModel(
                        it.name,
                        it.absolutePath,
                        it.isDirectory,
                        Date(attrs.creationTime().toMillis()),
                        if (it.isDirectory) folderSize(it) else it.length()
                    ),
                    FileProfileStatus.SUCCESS
                )
            } ?: emptyList())
    }

    fun folderSize(directory: File): Long {
        var length: Long = 0
        directory.listFiles() ?: return length
        for (file in directory.listFiles()) {
            if (file.isFile)
                length += file.length()
            else
                length += folderSize(file)
        }
        return length
    }

    override fun getFileList(path: String): List<FileModelProfile> =

        File(path).listFiles()
            ?.toList()
            ?.map {
                val attrs = Files.readAttributes(it.toPath(), BasicFileAttributes::class.java);

                FileModelProfile(
                    FileModel(
                        it.name,
                        it.absolutePath,
                        it.isDirectory,
                        Date(attrs.creationTime().toMillis()),
                        it.length()
                    ), FileProfileStatus.SUCCESS
                )
            } ?: emptyList()


    override fun watchDirectory(path: String): Observable<WatchEvent<*>> =
        PathObservables
            .watchNonRecursive(Paths.get(path))
            .subscribeOn(Schedulers.io())

}
