package com.test.ktor

import io.ktor.application.Application
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty


fun main(args: Array<String>) {
    embeddedServer(
        Netty, port = 8080,
        module = Application::mainModule
    ).start(wait = true)
}

